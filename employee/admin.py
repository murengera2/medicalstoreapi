from django.contrib import admin

from  employee.models import Employee,EmployeeBank,EmployeeSalary

admin.site.register(EmployeeSalary)
admin.site.register(Employee)
admin.site.register(EmployeeBank)