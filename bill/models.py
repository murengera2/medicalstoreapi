from django.db import models
import uuid
from customer.models import Customer
from medicine.models import Medicine


class Bill(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    customer=models.ForeignKey(Customer,on_delete=models.CASCADE)
    added_on=models.DateTimeField(auto_now_add=True)


class BillDetail(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    bill=models.ForeignKey(Bill,on_delete=models.CASCADE)
    medicine=models.ForeignKey(Medicine,on_delete=models.CASCADE)
    qty=models.IntegerField()
    added_on=models.DateTimeField(auto_now_add=True)

